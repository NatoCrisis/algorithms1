import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.princeton.cs.algs4.StdRandom;

public class RandomizedQueue<Item> implements Iterable<Item> {

	private Object[] items;
	private int index;

	public RandomizedQueue() {
		items = new Object[10];
		index = 0;
	}

	public boolean isEmpty() {
		return false;
	}

	public int size() {
		return index + 1;
	}

	public void enqueue(Item item) {
		if (item == null) {
			throw new IllegalArgumentException("Cannot enqueue a null object");
		}

		if (index == 0) {
			items[index++] = item;
		} else {
			int insertAt;
			if (index != 0) {
				insertAt = StdRandom.uniform(0, index);
			} else {
				insertAt = 0;
			}

			Item pulled = (Item) items[insertAt];
			items[insertAt] = item;
			items[index++] = pulled;
		}

		if (index == items.length - 1) {
			growArray();
		}
	}

	public Item dequeue() {
		if (index == 0) {
			throw new NoSuchElementException();
		}

		Item toRemove = (Item) items[--index];

		if (toRemove == null) {
			throw new NoSuchElementException();
		}

		items[index] = null;

		if (index > 0 && index <= items.length / 4) {
			shrinkArray();
		}

		return toRemove;
	}

	public Item sample() {
		if (index == 0) {
			throw new NoSuchElementException();
		}

		int idx;
		if (index != 0) {
			idx = StdRandom.uniform(0, index);
		} else {
			idx = 0;
		}

		return (Item) items[idx];
	}

	@Override
	public Iterator<Item> iterator() {
		
		StdRandom.shuffle(items, 0, index);

		return new Iterator<Item>() {

			private int start = 0;

			@Override
			public boolean hasNext() {
				return start < items.length && items[start + 1] != null;
			}

			@Override
			public Item next() {
				return (Item) items[start++];
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
		
	}

	private void growArray() {
		Object[] resized = new Object[index * 2];

		for (int i = 0; i < items.length; i++) {
			resized[i] = items[i];
		}

		items = resized;
	}

	private void shrinkArray() {
		if (items.length <= 10) {
			return;
		}

		Object[] resized = new Object[items.length / 2];

		for (int i = 0; i < resized.length; i++) {
			resized[i] = items[i];
		}

		items = resized;
	}

}
