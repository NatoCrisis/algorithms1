import static java.lang.Math.sqrt;

import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

public class PercolationStats {

	private final int n;
	private final int trials;
	private final double[] estimatedPercolation;
	private final double CONFIDENCE_LEVEL = 1.96;
	
	public PercolationStats(int n, int trials) {
		if(n <= 0 || trials <= 0) {
			throw new IllegalArgumentException("PercolationStats cannot accept values of zero or less");
		}
		
		this.n = n;
		this.trials = trials;
		this.estimatedPercolation = new double[trials];
		runTrails();
	}
	
	private void runTrails() {
		for(int trial = 0; trial < trials; trial++ ) {
			Percolation percolation = new Percolation(n);
			double opened = 0;
			while(!percolation.percolates()) {
				
				int row = StdRandom.uniform(n) + 1;
				int col = StdRandom.uniform(n) + 1;
				if(!percolation.isOpen(row, col)) {
					percolation.open(row, col);
					opened++;
				}
			}
			
			estimatedPercolation[trial] = opened / (n * n);
		}
	}

	public static void main(String[] args) {
		
		int gridSize = Integer.parseInt(args[0]);
		int totalTrials = Integer.parseInt(args[1]);
		
		PercolationStats percolationStats = new PercolationStats(gridSize, totalTrials);
		StdOut.println("mean\t\t\t= " + percolationStats.mean());
		StdOut.println("stddev\t\t\t= " + percolationStats.stddev());
		StdOut.println("95% confidence interval\t= [" + percolationStats.confidenceLo() + ", " + percolationStats.confidenceHi() + "]");

	}

	 public double mean() {
		 return StdStats.mean(estimatedPercolation);
	 }

	public double stddev() {
		return StdStats.stddev(estimatedPercolation);
	}

	// low endpoint of 95% confidence interval
	public double confidenceLo() {
		return mean() - CONFIDENCE_LEVEL * stddev() / sqrt(trials);
	}

	// high endpoint of 95% confidence interval
	public double confidenceHi() {
		return mean() + CONFIDENCE_LEVEL * stddev() / sqrt(trials);
	}
	
}
