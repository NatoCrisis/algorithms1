import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

public class Permutation {

	public static void main(String[] args) {

		int k = Integer.parseInt(args[0]);

		String[] items = StdIn.readAllLines();

		RandomizedQueue<String> queue = new RandomizedQueue<String>();

		for (int i = 0; i < k; i++) {
			queue.enqueue(items[i]);
		}
		
		for (int i = 0; i < k; i++) {
			StdOut.println(queue.dequeue());
		}
	}
}
