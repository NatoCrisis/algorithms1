import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {

	private final boolean[][] grid;
	private final int length;
	private int openSites;
	
	private final int virtualBottom;
	private final WeightedQuickUnionUF unionFind;
	
	// create n-by-n grid, with all sites blocked
	public Percolation(int n) {
		if (n <= 0)
			throw new IllegalArgumentException();

		length = n;
		openSites = 0;
		
		/* Init the data structure.
		 * Needs to be the same size as our grid, but two extra elements for the
		 * virtual top and bottom */
		unionFind = new WeightedQuickUnionUF((n * n) + 2);
		
		// The virtual bottom row will be the last element in the UF datatype
		virtualBottom = (length * length) + 1;

		grid = new boolean[n][n];
		for (int row = 0; row < n; row++) {
			for(int col = 0; col < n; col++) {
				grid[row][col] = false;
			}
		}
	}
	
	public boolean isOpen(int row, int col) {
		checkCoordinates(row, col);
		
		int rowOffset = row - 1;
		int colOffset = col - 1;
		
		return grid[rowOffset][colOffset];
	}

	public boolean isFull(int row, int col) {
		checkCoordinates(row, col);
		
		int rowOffset = row - 1;
		int colOffset = col - 1;
		
		return !grid[rowOffset][colOffset];
	}

	public int numberOfOpenSites() {
		return openSites;
	}

	public boolean percolates() {
		return unionFind.connected(0, (length * length)  + 1);
	}

	public void open(int row, int col) {
		checkCoordinates(row, col);

		if(!isOpen(row, col)) {
			int rowOffset = row - 1;
			int colOffset = col - 1;
			
			grid[rowOffset][colOffset] = true;
			openSites++;
			unionLeft(row, col);
			unionTop(row, col);
			unionRight(row, col);
			unionBottom(row, col);
			unionVirtualTop(row, col);
			unionVirtualBottom(row, col);
		}
	}

	private void unionVirtualBottom(int row, int col) {
		if(row == length) {
			int unionTo = (length * length) - (length - col);
			unionFind.union(virtualBottom, unionTo);
		}
	}

	private void unionVirtualTop(int row, int col) {
		if(row == 1) {
			unionFind.union(0, row * col);
		}
	}

	private void unionLeft(int row, int col) {
		// If col is gt than one, then we are not at the left edge of the grid
		if( col > 1 && isOpen(row, col -1)){
			int unionFrom = calculateUnionFrom(row, col);
			int unionTo = unionFrom - 1;
			
			unionFind.union( unionFrom, unionTo );
		}
	}
	
	private void unionTop(int row, int col) {
		// If the row is gt than one, then we are not at the top of the grid
		if(row > 1 && isOpen(row - 1, col)) {
			int unionFrom = calculateUnionFrom(row, col);
			int unionTo = unionFrom - length;
			
			unionFind.union( unionFrom , unionTo);
		}
	}
	
	private void unionRight(int row, int col) {
		// If the col is less than the width of the grid, 
		//	then we are not at the right edge
		if( col < length && isOpen(row, col + 1)) {
			int unionFrom = calculateUnionFrom(row, col);
			int unionTo = unionFrom + 1;
			
			unionFind.union(unionFrom, unionTo);
		}
	}
	
	private void unionBottom(int row, int col) {
		// if the row is less then the length, then we are not at the bottom row
		if(row < length && isOpen(row + 1, col)) {
			int unionFrom = calculateUnionFrom(row, col);
			int unionTo = unionFrom + length;
			
			unionFind.union(unionFrom, unionTo);
		}
	}

	/**
	 * Calculate where we want to union from.
	 * Start by calculating the end of the row (row * length)
	 * Then calculate backwards to the column we want (length - col)
	 */
	private int calculateUnionFrom(int row, int col) {
		return (row * length) - (length - col);
	}

	private void checkCoordinates(int row, int col) {
		if (row <= 0 || col <= 0) {
			throw new IllegalArgumentException();
		}
		if (row > length || col > length) {
			throw new IllegalArgumentException();
		}
	}
}
