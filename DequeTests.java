import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

public class DequeTests {

	Deque<String> d;
	@Before
	public void setUp() throws Exception {
		d = new Deque<String>();
	}
	
	@Test
	public void testAddFirstRemoveFirst() {
		d.addFirst("first");
		d.addFirst("second");
		
		assertEquals("second", d.removeFirst());
		assertEquals("first", d.removeFirst());
	}
	
	@Test
	public void testAddLastRemoveLast() {
		d.addLast("first");
		d.addLast("second");
		
		assertEquals("second", d.removeLast());
		assertEquals("first", d.removeLast());
	}
	
	@Test
	public void testAddFirstRemoveLast() {
		d.addFirst("first");
		d.addFirst("second");
		
		assertEquals("first", d.removeLast());
		assertEquals("second", d.removeLast());
	}
	
	@Test
	public void testAddLastRemoveFirst() {
		d.addLast("first");
		d.addLast("second");
		
		assertEquals("first", d.removeFirst());
		assertEquals("second", d.removeFirst());
	}
	
	@Test
	public void testAddBothEndsRemoveFirst() {
		d.addFirst("2");
		d.addFirst("1");
		d.addLast("3");
		d.addLast("4");
		
		assertEquals("1", d.removeFirst());
		assertEquals("2", d.removeFirst());
		assertEquals("3", d.removeFirst());
		assertEquals("4", d.removeFirst());
	}
	
	@Test
	public void testAddBothEndsRemoveLast() {
		d.addFirst("2");
		d.addFirst("1");
		d.addLast("3");
		d.addLast("4");
		
		assertEquals("4", d.removeLast());
		assertEquals("3", d.removeLast());
		assertEquals("2", d.removeLast());
		assertEquals("1", d.removeLast());
	}
	
	@Test
	public void testGrowArrayFromAddFirst() {
		final int growTo = 100;
		for(int i = 1 ; i <= growTo; i++) {
			d.addFirst("" + i);
		}
		
		assertEquals(growTo, d.size());
	}
	
	@Test
	public void testGrowArrayFromAddLast() {
		final int growTo = 100;
		for(int i = 1 ; i <= growTo; i++) {
			d.addLast("" + i);
		}
		
		assertEquals(growTo, d.size());
	}
	
	@Test
	public void testGrowArrayFromBothEnds() {
		final int growTo = 100;
		for(int i = 1; i <= growTo; i++) {
			
			if(i % 2 == 0)
				d.addFirst("" + 1);
			else
				d.addLast("" + 1);
		}
		
		assertEquals(growTo, d.size());
	}
	
	@Test
	public void testMultipleGrowAndRemoves() {
		final int growTo = 100;
		for(int i = 1 ; i <= growTo; i++) {
			d.addFirst("" + 1);
		}
		
		for(int i = 1 ; i <= growTo; i++) {
			d.removeFirst();
		}
		
		
		for(int i = 1 ; i <= growTo; i++) {
			d.addFirst("" + 1);
		}
		
		for(int i = 1 ; i <= growTo; i++) {
			d.removeFirst();
		}
		
		assertEquals(0, d.size());
		
	}
	
	@Test
	public void shrinkArrayFromRemoveFirst() {
		
		final int growTo = 150;
		final int shrinkTo = 10;
		
		for(int i = 1 ; i <= growTo; i++) {
			d.addFirst("" + i);
		}
		
		int lastIteration = 0;
		try {
			for(int i = 0; i < (growTo - shrinkTo); i++) {
				lastIteration = i;
				d.removeFirst();
			}
		} catch (Exception e) {
			fail("Failed at iteration " + lastIteration);
		}
		
		assertEquals(shrinkTo, d.size());
	}
	
	public void testShrinkArrayFromRemoveLast() {
		for(int i = 1 ; i <= 20; i++) {
			d.addFirst("" + i);
		}
		
		for(int i = 0; i < 10; i++) {
			d.removeLast();
		}
		
		assertEquals(10, d.size());
	}

	@Test
	public void testIteratorAfterAddFirst() {
		Deque<String> d = new Deque<String>();
		
		final int runTo = 201;
		
		for(int i = 0; i < runTo; i++) {
			String add = (i + 1) + "";
			d.addFirst(add);
			assertTrue(d.size() == (i + 1));
			
		}
		
		Iterator<String> it = d.iterator();
		int i = runTo;
		while(it.hasNext()) {
			String c = i + "";
			String next = it.next();
			assertTrue("next was " + next + " and c was " + c , next.equals(c));
			i--;
		}
	}
	
	@Test
	public void testIteratorAfterAddLast() {
		Deque<String> d = new Deque<String>();

		for (int i = 0; i < 201; i++) {
			String add = (i + 1) + "";
			d.addLast(add);
			assertTrue(d.size() == (i + 1));

		}

		Iterator<String> it = d.iterator();
		int i = 1;
		while (it.hasNext()) {
			String c = i + "";
			String next = it.next();
			assertTrue("next was " + next + " and c was " + c, next.equals(c));
			i++;
		}
	}
}
