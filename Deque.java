import java.util.Iterator;
import java.util.NoSuchElementException;

public class Deque<Item> implements Iterable<Item> {

	private Object[] items;

	private int front;
	private int back;
	private int itemCount;

	public Deque() {
		items = new Object[10];

		front = 0;
		back = 0;
		itemCount = 0;
	}

	public boolean isEmpty() {
		return itemCount > 0;
	}

	public int size() {
		return itemCount;
	}

	public void addFirst(Item item) {
		if (item == null) {
			throw new IllegalArgumentException("Cannot add null item");
		}

		items[front--] = item;
		itemCount++;

		if (front < 0) {
			front = items.length - 1;
		}

		if (itemCount == items.length - 1) {
			growArray();
		}
	}

	public Item removeFirst() {
		final Item toRemove = popFirst();

		if (itemCount > 0 && itemCount <= items.length / 4) {
			shrinkArray();
		}

		return toRemove;
	}

	public void addLast(Item item) {
		if (item == null) {
			throw new IllegalArgumentException("Cannot add null item");
		}

		items[++back] = item;
		itemCount++;

		if (itemCount == items.length - 1) {
			growArray();
		}
	}

	public Item removeLast() {
		Item toRemove = (Item) items[back];

		if (toRemove == null) {
			throw new NoSuchElementException();
		}

		items[back--] = null;
		itemCount--;

		if (back < 0) {
			back = items.length - 1;
		}

		if (itemCount > 10 && itemCount <= items.length / 4) {
			shrinkArray();
		}

		return toRemove;
	}

	private Item popFirst() {
		if (++front >= items.length) {
			front = 0;
		}

		final Item toRemove = (Item) items[front];

		if (toRemove == null) {
			throw new NoSuchElementException();
		}

		items[front] = null;
		itemCount--;

		return toRemove;
	}

	@Override
	public Iterator<Item> iterator() {

		// 8
		return new Iterator<Item>() {
			// 4
			int iteratorFront = front;

			@Override
			public boolean hasNext() {
				int next;
				if (iteratorFront + 2 >= items.length) {
					next = 0;
				} else {
					next = iteratorFront + 1;
				}

				return items[next] != null;
			}

			@Override
			public Item next() {
				if (++iteratorFront >= items.length) {
					iteratorFront = 0;
				}

				Item nextItem = (Item) items[iteratorFront];

				if (nextItem == null) {
					throw new NoSuchElementException();
				}

				return nextItem;
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException("Remove is not supported for this iterator");
			}
		};
	}

	private void growArray() {
		int growToSize = items.length * 2;
		Object[] resized = new Object[growToSize];
		int idx = 0;

		final int originalItemCount = itemCount;
		while (itemCount != 0) {
			Object obj = popFirst();
			resized[idx++] = obj;
		}

		itemCount = originalItemCount;
		items = resized;
		front = items.length - 1;
		back = idx - 1;
	}

	private void shrinkArray() {
		if (items.length <= 10) {
			return;
		}

		final int shrinkToSize = items.length / 2;
		Object[] resized = new Object[shrinkToSize];

		int idx = 0;

		final int originalItemCount = itemCount;
		while (itemCount != 0) {
			final Object obj = popFirst();
			resized[idx++] = obj;
		}

		itemCount = originalItemCount;
		items = resized;
		front = items.length - 1;
		back = idx;

	}
}