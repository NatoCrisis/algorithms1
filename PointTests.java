import static org.junit.Assert.*;

import java.util.Comparator;

import org.junit.Before;
import org.junit.Test;

public class PointTests {

	Point point;

	/*
	 * ***********************************************************************
	 * ** TEST COMPARE TO
	 **********************************************************************/
	// Formally, the invoking point (x0, y0) is less than the argument point
	// (x1, y1) if and only if either y0 < y1 or if y0 = y1 and x0 < x1

	@Before
	public void setUp() throws Exception {
		point = new Point(5, 5);
	}

	@Test
	public void testCompareToIsEqual() {
		Point other = new Point(5, 5);
		assertEquals(0, point.compareTo(other));
	}

	@Test
	public void testCompareToIsGreather() {
		Point other = new Point(1, 1);
		assertEquals(1, point.compareTo(other));
	}

	@Test
	public void testCompareToIsLess() {
		Point other = new Point(10, 10);
		assertEquals(-1, point.compareTo(other));
	}

	@Test
	public void testCompareToIsGreaterWhenUseingXAsTieBreaker() {
		// the invoker has a greater value for x, therefore should be greather
		// that the arguement
		Point other = new Point(4, 5);
		assertEquals(1, point.compareTo(other));
	}

	@Test
	public void testCompareToIsLessWhenUseingXAsTieBreaker() {
		// the invoker has a lesser value for x, therefore should be less that
		// the arguement
		Point other = new Point(6, 5);
		assertEquals(-1, point.compareTo(other));
	}

	/*
	 * ***********************************************************************
	 * ** TEST SLOPE TO
	 **********************************************************************/
	/*
	 * The slopeTo() method should return the slope between the invoking point
	 * (x0, y0) and the argument point (x1, y1), which is given by the formula
	 * (y1 − y0) / (x1 − x0). Treat the slope of a horizontal line segment as
	 * positive zero; treat the slope of a vertical line segment as positive
	 * infinity; treat the slope of a degenerate line segment (between a point
	 * and itself) as negative infinity.
	 */

	@Test
	public void testVerticalLineSegment1() {
		int x0 = 1;
		int x1 = 1;

		int y0 = 1;
		int y1 = 10;

		Point zero = new Point(x0, y0);
		Point one = new Point(x1, y1);

		assertEquals(Double.POSITIVE_INFINITY, zero.slopeTo(one), 0.001);
	}

	@Test
	public void testVerticalLineSegment2() {
		int x0 = 1;
		int x1 = 1;

		int y0 = -1;
		int y1 = 10;

		Point zero = new Point(x0, y0);
		Point one = new Point(x1, y1);

		assertEquals(Double.POSITIVE_INFINITY, zero.slopeTo(one), 0.001);
	}

	@Test
	public void testVerticalLineSegment3() {
		int x0 = -1;
		int x1 = -1;

		int y0 = -1;
		int y1 = 10;

		Point zero = new Point(x0, y0);
		Point one = new Point(x1, y1);

		assertEquals(Double.POSITIVE_INFINITY, zero.slopeTo(one), 0.001);
	}

	@Test
	public void testVerticalLineSegment4() {
		int x0 = -1;
		int x1 = -1;

		int y0 = -1;
		int y1 = -10;

		Point zero = new Point(x0, y0);
		Point one = new Point(x1, y1);

		assertEquals(Double.POSITIVE_INFINITY, zero.slopeTo(one), 0.001);
	}

	@Test
	public void testDegenerateLineSegment() {
		Point other = new Point(5, 5);

		assertEquals(Double.NEGATIVE_INFINITY, point.slopeTo(other), 0.001);
	}

	@Test
	public void testBasicCalculation() {
		int x1 = 5;
		int x0 = 8;

		int y1 = 3;
		int y0 = 9;

		double result = (y1 - y0) / (x1 - x0);

		Point one = new Point(x1, y1);
		Point zero = new Point(x0, y0);

		assertEquals(result, zero.slopeTo(one), 0.001);
	}

	@Test
	public void testHorizontalLineSegment1() {
		int x1 = 5;
		int x0 = 1;

		int y1 = 1;
		int y0 = 1;

		Point one = new Point(x1, y1);
		Point zero = new Point(x0, y0);

		assertEquals(0, zero.slopeTo(one), 0.001);
	}

	@Test
	public void testHorizontalLineSegment2() {
		int x1 = 5;
		int x0 = -1;

		int y1 = 1;
		int y0 = 1;

		Point one = new Point(x1, y1);
		Point zero = new Point(x0, y0);

		assertEquals(-0, zero.slopeTo(one), 0.001);
	}

	@Test
	public void testHorizontalLineSegment3() {
		int x1 = 5;
		int x0 = -1;

		int y1 = -1;
		int y0 = -1;

		Point one = new Point(x1, y1);
		Point zero = new Point(x0, y0);

		assertEquals(0, zero.slopeTo(one), 0.001);
	}

	@Test
	public void testHorizontalLineSegment4() {
		int x1 = -5;
		int x0 = -1;

		int y1 = -1;
		int y0 = -1;

		Point one = new Point(x1, y1);
		Point zero = new Point(x0, y0);

		assertEquals(0, zero.slopeTo(one), 0.001);
	}

	/*
	 * ***********************************************************************
	 * ** TEST SLOPE ORDER
	 **********************************************************************/
	/*
	 * The slopeOrder() method should return a comparator that compares its two
	 * argument points by the slopes they make with the invoking point (x0, y0).
	 * Formally, the point (x1, y1) is less than the point (x2, y2) if and only
	 * if the slope (y1 − y0) / (x1 − x0) is less than the slope (y2 − y0) / (x2
	 * − x0). Treat horizontal, vertical, and degenerate line segments as in the
	 * slopeTo() method.
	 */

	@Test
	public void testBasicComparatorIsLess() {
		int x0 = 5;
		int x1 = 9;
		int x2 = 12;

		int y0 = 5;
		int y1 = 1;
		int y2 = 1;

		Point zero = new Point(x0, y0);
		Point one = new Point(x1, y1);
		Point two = new Point(x2, y2);

		Comparator<Point> comparator = zero.slopeOrder();

		assertEquals(-1, comparator.compare(one, two));
	}

	@Test
	public void testBasicComparatorIsGreater() {
		int x0 = 1;
		int x1 = 2;
		int x2 = 15;

		int y0 = 1;
		int y1 = 15;
		int y2 = 3;

		Point zero = new Point(x0, y0);
		Point one = new Point(x1, y1);
		Point two = new Point(x2, y2);

		Comparator<Point> comparator = zero.slopeOrder();

		assertEquals(1, comparator.compare(one, two));
	}

	@Test
	public void testBasicComparatorIsEqualWhenHorizontal() {
		int x0 = 1;
		int x1 = 5;
		int x2 = 15;

		int y0 = 1;
		int y1 = 1;
		int y2 = 1;

		Point zero = new Point(x0, y0);
		Point one = new Point(x1, y1);
		Point two = new Point(x2, y2);

		Comparator<Point> comparator = zero.slopeOrder();

		assertEquals(0, comparator.compare(one, two));
	}

	public void testBasicComparatorIsEqualWhenVertical() {
		int x0 = 1;
		int x1 = 1;
		int x2 = 1;

		int y0 = 1;
		int y1 = 5;
		int y2 = 25;

		Point zero = new Point(x0, y0);
		Point one = new Point(x1, y1);
		Point two = new Point(x2, y2);

		Comparator<Point> comparator = zero.slopeOrder();

		assertEquals(0, comparator.compare(one, two));
	}

}
