import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.NoSuchElementException;

import org.junit.Before;
import org.junit.Test;

public class TestRandomizedQueue {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testAddNullThrowsException() {
		RandomizedQueue<String> r = new RandomizedQueue<String>();
		
		try {
			r.enqueue(null);
			fail();
		} catch(IllegalArgumentException e) {
			assertTrue(true);
		}
		
	}
	
	@Test
	public void testDequeueThrowsException() {
		RandomizedQueue<String> r = new RandomizedQueue<String>();

		try {
			r.dequeue();
			fail();
		} catch (NoSuchElementException e) {
			assertTrue(true);
		}
	}

}
